//================= Hercules Database =====================================
//=       _   _                     _
//=      | | | |                   | |
//=      | |_| | ___ _ __ ___ _   _| | ___  ___
//=      |  _  |/ _ \ '__/ __| | | | |/ _ \/ __|
//=      | | | |  __/ | | (__| |_| | |  __/\__ \
//=      \_| |_/\___|_|  \___|\__,_|_|\___||___/
//================= License ===============================================
//= This file is part of Hercules.
//= http://herc.ws - http://github.com/HerculesWS/Hercules
//=
//= Copyright (C) 2018  Hercules Dev Team
//=
//= Hercules is free software: you can redistribute it and/or modify
//= it under the terms of the GNU General Public License as published by
//= the Free Software Foundation, either version 3 of the License, or
//= (at your option) any later version.
//=
//= This program is distributed in the hope that it will be useful,
//= but WITHOUT ANY WARRANTY; without even the implied warranty of
//= MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//= GNU General Public License for more details.
//=
//= You should have received a copy of the GNU General Public License
//= along with this program.  If not, see <http://www.gnu.org/licenses/>.
//=========================================================================
//= Pets Database
//=========================================================================

pet_db:(
/**************************************************************************
 ************* Entry structure ********************************************
 **************************************************************************
{
	// ================ Mandatory fields ==============================
	Id: ID                               (int)
	SpriteName: "Sprite_Name"            (string)
	Name: "Pet Name"                     (string)
	// ================ Optional fields ===============================
	TamingItem: Taming Item              (string, defaults to 0)
	EggItem: Egg Id                      (string, defaults to 0)
	AccessoryItem: Equipment Id          (string, defaults to 0)
	FoodItem: Food Id                    (string, defaults to 0)
	FoodEffectiveness: hunger points     (int, defaults to 0)
	HungerDelay: hunger time             (int, defaults to 0)
	Intimacy: {
		Initial: start intimacy                   (int, defaults to 0)
		FeedIncrement: feeding intimacy           (int, defaults to 0)
		OverFeedDecrement: overfeeding intimacy   (int, defaults to 0)
		OwnerDeathDecrement: owner die intimacy   (int, defaults to 0)
	}
	CaptureRate: capture rate            (int, defaults to 0)
	Speed: speed                         (int, defaults to 0)
	SpecialPerformance: true/false       (boolean, defaults to false)
	TalkWithEmotes: convert talk         (boolean, defaults to false)
	AttackRate: attack rate              (int, defaults to 0)
	DefendRate: Defence attack           (int, defaults to 0)
	ChangeTargetRate: change target      (int, defaults to 0)
	PetScript: <" Pet Script (can also be multi-line) ">
	EquipScript: <" Equip Script (can also be multi-line) ">
},
**************************************************************************/
	// entries in this file will override the ones in /(pre-)re/pet_db.conf

{
	Id: 21000
	SpriteName: "PYRON"
	Name: "Pyron"
	TamingItem: "Unripe_Apple"
	EggItem: "Pyron_Egg"
	FoodItem: "Apple_Juice"
	FoodEffectiveness: 100
	HungerDelay: 360000
	Intimacy: {
		Initial: 1000
		FeedIncrement: 1000
		OverFeedDecrement: 0
		OwnerDeathDecrement: 0
	}
	CaptureRate: 2000
	Speed: 150
	SpecialPerformance: true
	AttackRate: 1000
	DefendRate: 400
	ChangeTargetRate: 0
	PetScript: <" 

	">
	EquipScript: <"
	bonus3 bAddClassDropItem,36003,21000,10000;
	">
},
/*
{
	Id: 21017
	SpriteName: "PET_ANC_01"
	Name: "Ancient Pet"
	EggItem: "Pet_Ancient_Egg_1"
	FoodItem: "Food_Pet_Potion"
	FoodEffectiveness: 100
	HungerDelay: 120000
	Intimacy: {
		Initial: 1000
		FeedIncrement: 100
		OverFeedDecrement: 50
		OwnerDeathDecrement: 10
	}
	CaptureRate: 2000
	Speed: 150
	SpecialPerformance: true
	AttackRate: 1000
	DefendRate: 400
	ChangeTargetRate: 0
	PetScript: <" 

	">
	EquipScript: <"
	bonus bHit,200;
	bonus bFlee,200;

	">
},
*/
{
	Id: 21018
	SpriteName: "PET_UNIQ_01"
	Name: "Unique Pet"
	EggItem: "Pet_Unique_Egg_1"
	FoodItem: "Food_Pet_Potion"
	FoodEffectiveness: 100
	HungerDelay: 120000
	Intimacy: {
		Initial: 1000
		FeedIncrement: 100
		OverFeedDecrement: 50
		OwnerDeathDecrement: 10
	}
	CaptureRate: 2000
	Speed: 150
	SpecialPerformance: true
	AttackRate: 1000
	DefendRate: 400
	ChangeTargetRate: 0
	PetScript: <" 

	">
	EquipScript: <"
	bonus bHit,180;
	bonus bFlee,180;
	">
},

{
	Id: 21019
	SpriteName: "PET_UNIQ_02"
	Name: "Unique Pet"
	EggItem: "Pet_Unique_Egg_2"
	FoodItem: "Food_Pet_Potion"
	FoodEffectiveness: 100
	HungerDelay: 120000
	Intimacy: {
		Initial: 1000
		FeedIncrement: 100
		OverFeedDecrement: 50
		OwnerDeathDecrement: 10
	}
	CaptureRate: 2000
	Speed: 150
	SpecialPerformance: true
	AttackRate: 1000
	DefendRate: 400
	ChangeTargetRate: 0
	PetScript: <" 

	">
	EquipScript: <"
	bonus bHit,180;
	bonus bFlee,180;
	">
},

{
	Id: 21020
	SpriteName: "PET_RARE_01"
	Name: "Rare Pet"
	EggItem: "Pet_Rare_Egg_1"
	FoodItem: "Food_Pet_Potion"
	FoodEffectiveness: 100
	HungerDelay: 120000
	Intimacy: {
		Initial: 1000
		FeedIncrement: 100
		OverFeedDecrement: 50
		OwnerDeathDecrement: 10
	}
	CaptureRate: 2000
	Speed: 150
	SpecialPerformance: true
	AttackRate: 1000
	DefendRate: 400
	ChangeTargetRate: 0
	PetScript: <" 

	">
	EquipScript: <"
	bonus bHit,160;
	bonus bFlee,160;
	">
},

{
	Id: 21021
	SpriteName: "PET_RARE_01"
	Name: "Rare Pet"
	EggItem: "Pet_Rare_Egg_2"
	FoodItem: "Food_Pet_Potion"
	FoodEffectiveness: 100
	HungerDelay: 120000
	Intimacy: {
		Initial: 1000
		FeedIncrement: 100
		OverFeedDecrement: 50
		OwnerDeathDecrement: 10
	}
	CaptureRate: 2000
	Speed: 150
	SpecialPerformance: true
	AttackRate: 1000
	DefendRate: 400
	ChangeTargetRate: 0
	PetScript: <" 

	">
	EquipScript: <"
	bonus bHit,160;
	bonus bFlee,160;
	">
},

{
	Id: 21022
	SpriteName: "PET_RARE_01"
	Name: "Rare Pet"
	EggItem: "Pet_Rare_Egg_3"
	FoodItem: "Food_Pet_Potion"
	FoodEffectiveness: 100
	HungerDelay: 120000
	Intimacy: {
		Initial: 1000
		FeedIncrement: 100
		OverFeedDecrement: 50
		OwnerDeathDecrement: 10
	}
	CaptureRate: 2000
	Speed: 150
	SpecialPerformance: true
	AttackRate: 1000
	DefendRate: 400
	ChangeTargetRate: 0
	PetScript: <" 

	">
	EquipScript: <"
	bonus bHit,160;
	bonus bFlee,160;
	">
},

{
	Id: 21023
	SpriteName: "PET_UNCOM_01"
	Name: "Uncommon Pet"
	EggItem: "Pet_Uncommon_Egg_1"
	FoodItem: "Food_Pet_Potion"
	FoodEffectiveness: 100
	HungerDelay: 120000
	Intimacy: {
		Initial: 1000
		FeedIncrement: 100
		OverFeedDecrement: 50
		OwnerDeathDecrement: 10
	}
	CaptureRate: 2000
	Speed: 150
	SpecialPerformance: true
	AttackRate: 1000
	DefendRate: 400
	ChangeTargetRate: 0
	PetScript: <" 

	">
	EquipScript: <"
	bonus bHit,140;
	bonus bFlee,140;
	">
},

{
	Id: 21024
	SpriteName: "PET_UNCOM_01"
	Name: "Uncommon Pet"
	EggItem: "Pet_Uncommon_Egg_2"
	FoodItem: "Food_Pet_Potion"
	FoodEffectiveness: 100
	HungerDelay: 120000
	Intimacy: {
		Initial: 1000
		FeedIncrement: 100
		OverFeedDecrement: 50
		OwnerDeathDecrement: 10
	}
	CaptureRate: 2000
	Speed: 150
	SpecialPerformance: true
	AttackRate: 1000
	DefendRate: 400
	ChangeTargetRate: 0
	PetScript: <" 

	">
	EquipScript: <"
	bonus bHit,140;
	bonus bFlee,140;
	">
},

{
	Id: 21025
	SpriteName: "PET_COM_01"
	Name: "Common Pet"
	EggItem: "Pet_Common_Egg_1"
	FoodItem: "Food_Pet_Potion"
	FoodEffectiveness: 100
	HungerDelay: 120000
	Intimacy: {
		Initial: 1000
		FeedIncrement: 100
		OverFeedDecrement: 50
		OwnerDeathDecrement: 10
	}
	CaptureRate: 2000
	Speed: 150
	SpecialPerformance: true
	AttackRate: 1000
	DefendRate: 400
	ChangeTargetRate: 0
	PetScript: <" 

	">
	EquipScript: <"
	bonus bHit,120;
	bonus bFlee,120;
	">
},


{
	Id: 21026
	SpriteName: "PET_COM_01"
	Name: "Common Pet"
	EggItem: "Pet_Common_Egg_2"
	FoodItem: "Food_Pet_Potion"
	FoodEffectiveness: 100
	HungerDelay: 120000
	Intimacy: {
		Initial: 1000
		FeedIncrement: 100
		OverFeedDecrement: 50
		OwnerDeathDecrement: 10
	}
	CaptureRate: 2000
	Speed: 150
	SpecialPerformance: true
	AttackRate: 1000
	DefendRate: 400
	ChangeTargetRate: 0
	PetScript: <" 

	">
	EquipScript: <"
	bonus bHit,120;
	bonus bFlee,120;
	">
},

{
	Id: 21027
	SpriteName: "PET_COM_01"
	Name: "Common Pet"
	EggItem: "Pet_Common_Egg_3"
	FoodItem: "Food_Pet_Potion"
	FoodEffectiveness: 100
	HungerDelay: 120000
	Intimacy: {
		Initial: 1000
		FeedIncrement: 100
		OverFeedDecrement: 50
		OwnerDeathDecrement: 10
	}
	CaptureRate: 2000
	Speed: 150
	SpecialPerformance: true
	AttackRate: 1000
	DefendRate: 400
	ChangeTargetRate: 0
	PetScript: <" 

	">
	EquipScript: <"
	bonus bHit,120;
	bonus bFlee,120;
	">
},

{
	Id: 21028
	SpriteName: "PET_ANC_01"
	Name: "Ancient Pet"
	EggItem: "Pet_Ancient_Egg_2"
	FoodItem: "Food_Pet_Potion"
	FoodEffectiveness: 100
	HungerDelay: 120000
	Intimacy: {
		Initial: 1000
		FeedIncrement: 100
		OverFeedDecrement: 50
		OwnerDeathDecrement: 10
	}
	CaptureRate: 2000
	Speed: 150
	SpecialPerformance: true
	AttackRate: 1000
	DefendRate: 400
	ChangeTargetRate: 0
	PetScript: <" 

	">
	EquipScript: <"
	bonus bHit,220;
	bonus bFlee,220;
	">
},


{
	Id: 21029
	SpriteName: "PET_ANC_01"
	Name: "Ancient Pet"
	EggItem: "Pet_Ancient_Egg_3"
	FoodItem: "Food_Pet_Potion"
	FoodEffectiveness: 100
	HungerDelay: 120000
	Intimacy: {
		Initial: 1000
		FeedIncrement: 100
		OverFeedDecrement: 50
		OwnerDeathDecrement: 10
	}
	CaptureRate: 2000
	Speed: 150
	SpecialPerformance: true
	AttackRate: 1000
	DefendRate: 400
	ChangeTargetRate: 0
	PetScript: <" 

	">
	EquipScript: <"
	bonus bHit,220;
	bonus bFlee,220;
	">
},

{
	Id: 21030
	SpriteName: "PET_ANC_01"
	Name: "Ancient Pet"
	EggItem: "Pet_Ancient_Egg_4"
	FoodItem: "Food_Pet_Potion"
	FoodEffectiveness: 100
	HungerDelay: 120000
	Intimacy: {
		Initial: 1000
		FeedIncrement: 100
		OverFeedDecrement: 50
		OwnerDeathDecrement: 10
	}
	CaptureRate: 2000
	Speed: 150
	SpecialPerformance: true
	AttackRate: 1000
	DefendRate: 400
	ChangeTargetRate: 0
	PetScript: <" 

	">
	EquipScript: <"
	bonus bHit,220;
	bonus bFlee,220;
	">
},

{
	Id: 21017
	SpriteName: "PET_ANC_04"
	Name: "Ancient Pet"
	EggItem: "Pet_Ancient_Egg_5"
	FoodItem: "Food_Pet_Potion"
	FoodEffectiveness: 100
	HungerDelay: 120000
	Intimacy: {
		Initial: 1000
		FeedIncrement: 100
		OverFeedDecrement: 50
		OwnerDeathDecrement: 10
	}
	CaptureRate: 2000
	Speed: 150
	SpecialPerformance: true
	AttackRate: 1000
	DefendRate: 400
	ChangeTargetRate: 0
	PetScript: <" 

	">
	EquipScript: <"
	bonus bHit,220;
	bonus bFlee,220;
	">
},


)
