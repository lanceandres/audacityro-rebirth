auda_city1,80,182,5	script	Salvage Collector Eric 	4_M_IAN,{
cutin("verus_ian04",2);
mes "^00008b[ Salvage Collector Eric ]^000000";
mes "Greetings "+strcharinfo(0)+" !";
sleep2 800;
next;
cutin("verus_ian04",2);
mes "^00008b[ Salvage Collector Eric ]^000000";
mes "Looks like you're carrying too much scarp..";
sleep2 800;
emotion e_heh;
mes "I can trade you something useful for it!";
sleep2 800;
next;
cutin("verus_ian04",2);
mes "^00008b[ Salvage Collector Eric ]^000000";
mes "What service would you like me to do for you?";
sleep2 800;
next;
switch(select("Salvage Equipments","Trade Fragments")){
	case 1:
	cutin("verus_ian04",2);
	mes "^00008b[ Salvage Collector Eric ]^000000";
	mes "Choose the rarity of equipments you'd like to salvage..";
	sleep2 800;
	next;
	cutin("verus_ian04",2);
	mes "^00008b[ Salvage Collector Eric ]^000000";
	mes "Common = 3 Dragon Soul";
	mes "Uncommon = 5 Dragon Soul";
	mes "Rare = 9 Dragon Soul";
	mes "Unique = 15 Dragon Soul";
	sleep2 800;
	next;
		switch(select("Common","Uncommon","Rare","Unique")){
			case 1:
			if(countitem(35016) > 0 || countitem(35017) > 0 || countitem(35018) > 0 || countitem(35019) > 0 || countitem(35020) > 0 || countitem(35021) > 0 || countitem(35022) > 0 || countitem(35023) > 0 || countitem(35024) > 0 || countitem(35025) > 0 || countitem(35026) > 0 || countitem(35027) > 0 || countitem(35028) > 0 || countitem(35029) > 0 || countitem(35030) > 0 || countitem(34011) > 0 || countitem(34012) > 0 || countitem(34013) > 0 || countitem(34014) > 0 || countitem(34015) > 0 || countitem(34016) > 0 || countitem(34017) > 0 || countitem(34018) > 0 || countitem(34211) > 0 || countitem(34212) > 0 || countitem(34213) > 0 || countitem(34214) > 0 || countitem(34215) > 0 || countitem(34216) > 0 || countitem(34217) > 0 || countitem(34218) > 0 || countitem(34411) > 0 || countitem(34412) > 0 || countitem(34413) > 0 || countitem(34414) > 0 || countitem(34415) > 0 || countitem(34416) > 0 || countitem(34417) > 0 || countitem(34418) > 0 || countitem(34811) > 0 || countitem(34812) > 0 || countitem(34813) > 0 || countitem(34814) > 0 || countitem(34815) > 0 || countitem(34816) > 0 || countitem(34817) > 0 || countitem(34818) > 0)
			{
				message strcharinfo(0),"Calculating..";
				sleep2 1500;
				message strcharinfo(0),"Salvaging..";
				/// WEAPON
				if(countitem(35016)) { getitem 7701,3*countitem(35016); }
				if(countitem(35017)) { getitem 7701,3*countitem(35017); }
				if(countitem(35018)) { getitem 7701,3*countitem(35018); }
				if(countitem(35019)) { getitem 7701,3*countitem(35019); }
				if(countitem(35020)) { getitem 7701,3*countitem(35020); }
				if(countitem(35021)) { getitem 7701,3*countitem(35021); }
				if(countitem(35022)) { getitem 7701,3*countitem(35022); }
				if(countitem(35023)) { getitem 7701,3*countitem(35023); }
				if(countitem(35024)) { getitem 7701,3*countitem(35024); }
				if(countitem(35025)) { getitem 7701,3*countitem(35025); }
				if(countitem(35026)) { getitem 7701,3*countitem(35026); }
				if(countitem(35027)) { getitem 7701,3*countitem(35027); }
				if(countitem(35028)) { getitem 7701,3*countitem(35028); }
				if(countitem(35029)) { getitem 7701,3*countitem(35029); }
				if(countitem(35030)) { getitem 7701,3*countitem(35030); }
				/// ARMOR
				if(countitem(34011)) { getitem 7701,3*countitem(34011); }
				if(countitem(34012)) { getitem 7701,3*countitem(34012); }
				if(countitem(34013)) { getitem 7701,3*countitem(34013); }
				if(countitem(34014)) { getitem 7701,3*countitem(34014); }
				if(countitem(34015)) { getitem 7701,3*countitem(34015); }
				if(countitem(34016)) { getitem 7701,3*countitem(34016); }
				if(countitem(34017)) { getitem 7701,3*countitem(34017); }
				if(countitem(34018)) { getitem 7701,3*countitem(34018); }
				/// SHIELD
				if(countitem(34211)) { getitem 7701,3*countitem(34211); }
				if(countitem(34212)) { getitem 7701,3*countitem(34212); }
				if(countitem(34213)) { getitem 7701,3*countitem(34213); }
				if(countitem(34214)) { getitem 7701,3*countitem(34214); }
				if(countitem(34215)) { getitem 7701,3*countitem(34215); }
				if(countitem(34216)) { getitem 7701,3*countitem(34216); }
				if(countitem(34217)) { getitem 7701,3*countitem(34217); }
				if(countitem(34218)) { getitem 7701,3*countitem(34218); }
				/// SHOES
				if(countitem(34411)) { getitem 7701,3*countitem(34411); }
				if(countitem(34412)) { getitem 7701,3*countitem(34412); }
				if(countitem(34413)) { getitem 7701,3*countitem(34413); }
				if(countitem(34414)) { getitem 7701,3*countitem(34414); }
				if(countitem(34415)) { getitem 7701,3*countitem(34415); }
				if(countitem(34416)) { getitem 7701,3*countitem(34416); }
				if(countitem(34417)) { getitem 7701,3*countitem(34417); }
				if(countitem(34418)) { getitem 7701,3*countitem(34418); }				
				/// MUFFLER
				if(countitem(34811)) { getitem 7701,3*countitem(34811); }
				if(countitem(34812)) { getitem 7701,3*countitem(34812); }
				if(countitem(34813)) { getitem 7701,3*countitem(34813); }
				if(countitem(34814)) { getitem 7701,3*countitem(34814); }
				if(countitem(34815)) { getitem 7701,3*countitem(34815); }
				if(countitem(34816)) { getitem 7701,3*countitem(34816); }
				if(countitem(34817)) { getitem 7701,3*countitem(34817); }
				if(countitem(34818)) { getitem 7701,3*countitem(34818); }



				/// WEAPON
				if(countitem(35016)) { delitem 35016,countitem(35016); }
				if(countitem(35017)) { delitem 35017,countitem(35017); }
				if(countitem(35018)) { delitem 35018,countitem(35018); }
				if(countitem(35019)) { delitem 35019,countitem(35019); }
				if(countitem(35020)) { delitem 35020,countitem(35020); }
				if(countitem(35021)) { delitem 35021,countitem(35021); }
				if(countitem(35022)) { delitem 35022,countitem(35022); }
				if(countitem(35023)) { delitem 35023,countitem(35023); }
				if(countitem(35024)) { delitem 35024,countitem(35024); }
				if(countitem(35025)) { delitem 35025,countitem(35025); }
				if(countitem(35026)) { delitem 35026,countitem(35026); }
				if(countitem(35027)) { delitem 35027,countitem(35027); }
				if(countitem(35028)) { delitem 35028,countitem(35028); }
				if(countitem(35029)) { delitem 35029,countitem(35029); }
				if(countitem(35030)) { delitem 35030,countitem(35030); }
				/// ARMOR
				if(countitem(34011)) { delitem 34011,countitem(34011); }
				if(countitem(34012)) { delitem 34012,countitem(34012); }
				if(countitem(34013)) { delitem 34013,countitem(34013); }
				if(countitem(34014)) { delitem 34014,countitem(34014); }
				if(countitem(34015)) { delitem 34015,countitem(34015); }
				if(countitem(34016)) { delitem 34016,countitem(34016); }
				if(countitem(34017)) { delitem 34017,countitem(34017); }
				if(countitem(34018)) { delitem 34018,countitem(34018); }
				/// SHIELD
				if(countitem(34211)) { delitem 34211,countitem(34211); }
				if(countitem(34212)) { delitem 34212,countitem(34212); }
				if(countitem(34213)) { delitem 34213,countitem(34213); }
				if(countitem(34214)) { delitem 34214,countitem(34214); }
				if(countitem(34215)) { delitem 34215,countitem(34215); }
				if(countitem(34216)) { delitem 34216,countitem(34216); }
				if(countitem(34217)) { delitem 34217,countitem(34217); }
				if(countitem(34218)) { delitem 34218,countitem(34218); }	
				/// SHOES
				if(countitem(34411)) { delitem 34411,countitem(34411); }
				if(countitem(34412)) { delitem 34412,countitem(34412); }
				if(countitem(34413)) { delitem 34413,countitem(34413); }
				if(countitem(34414)) { delitem 34414,countitem(34414); }
				if(countitem(34415)) { delitem 34415,countitem(34415); }
				if(countitem(34416)) { delitem 34416,countitem(34416); }
				if(countitem(34417)) { delitem 34417,countitem(34417); }
				if(countitem(34418)) { delitem 34418,countitem(34418); }	
				/// GARMENT
				if(countitem(34811)) { delitem 34811,countitem(34811); }
				if(countitem(34812)) { delitem 34812,countitem(34812); }
				if(countitem(34813)) { delitem 34813,countitem(34813); }
				if(countitem(34814)) { delitem 34814,countitem(34814); }
				if(countitem(34815)) { delitem 34815,countitem(34815); }
				if(countitem(34816)) { delitem 34816,countitem(34816); }
				if(countitem(34817)) { delitem 34817,countitem(34817); }
				if(countitem(34818)) { delitem 34818,countitem(34818); }			
				next;
				cutin("verus_ian04",2);
				mes "^00008b[ Salvage Collector Eric ]^000000";
				mes "Salvaging complete!";
				emotion e_cash;
				close2;
				cutin("", 255);
				end;
			}
			else
			cutin("verus_ian03",2);
			mes "^00008b[ Salvage Collector Eric ]^000000";
			mes "You don't have anything to salvage!";
			emotion e_bzz;
			sleep2 1000;
			close2;
			cutin("", 255);			
			end;
			case 2:
			if(countitem(35001) > 0 || countitem(35002) > 0 || countitem(35003) > 0 || countitem(35004) > 0 || countitem(35005) > 0 || countitem(35006) > 0 || countitem(35007) > 0 || countitem(35008) > 0 || countitem(35009) > 0 || countitem(35010) > 0 || countitem(35011) > 0 || countitem(35012) > 0 || countitem(35013) > 0 || countitem(35014) > 0 || countitem(35015) > 0 || countitem(34000) > 0 || countitem(34001) > 0 || countitem(34200) > 0 || countitem(34201) > 0 || countitem(34400) > 0 || countitem(34401) > 0 || countitem(34800) > 0 || countitem(34801) > 0)
			{
				message strcharinfo(0),"Calculating..";
				sleep2 1500;
				message strcharinfo(0),"Salvaging..";
				/// WEAPON
				if(countitem(35001)) { getitem 7701,5*countitem(35001); }
				if(countitem(35002)) { getitem 7701,5*countitem(35002); }
				if(countitem(35003)) { getitem 7701,5*countitem(35003); }
				if(countitem(35004)) { getitem 7701,5*countitem(35004); }
				if(countitem(35005)) { getitem 7701,5*countitem(35005); }
				if(countitem(35006)) { getitem 7701,5*countitem(35006); }
				if(countitem(35007)) { getitem 7701,5*countitem(35007); }
				if(countitem(35008)) { getitem 7701,5*countitem(35008); }
				if(countitem(35009)) { getitem 7701,5*countitem(35009); }
				if(countitem(35010)) { getitem 7701,5*countitem(35010); }
				if(countitem(35011)) { getitem 7701,5*countitem(35011); }
				if(countitem(35012)) { getitem 7701,5*countitem(35012); }
				if(countitem(35013)) { getitem 7701,5*countitem(35013); }
				if(countitem(35014)) { getitem 7701,5*countitem(35014); }
				if(countitem(35015)) { getitem 7701,5*countitem(35015); }
				/// ARMOR
				if(countitem(34000)) { getitem 7701,5*countitem(34000); }
				if(countitem(34001)) { getitem 7701,5*countitem(34001); }
				/// SHIELD
				if(countitem(34200)) { getitem 7701,5*countitem(34200); }
				if(countitem(34201)) { getitem 7701,5*countitem(34201); }
				/// SHOES
				if(countitem(34400)) { getitem 7701,5*countitem(34400); }
				if(countitem(34401)) { getitem 7701,5*countitem(34401); }			
				/// MUFFLER
				if(countitem(34800)) { getitem 7701,5*countitem(34800); }
				if(countitem(34801)) { getitem 7701,5*countitem(34801); }



				/// WEAPON
				if(countitem(35001)) { delitem 35001,countitem(35001); }
				if(countitem(35002)) { delitem 35002,countitem(35002); }
				if(countitem(35003)) { delitem 35003,countitem(35003); }
				if(countitem(35004)) { delitem 35004,countitem(35004); }
				if(countitem(35005)) { delitem 35005,countitem(35005); }
				if(countitem(35006)) { delitem 35006,countitem(35006); }
				if(countitem(35007)) { delitem 35007,countitem(35007); }
				if(countitem(35008)) { delitem 35008,countitem(35008); }
				if(countitem(35009)) { delitem 35009,countitem(35009); }
				if(countitem(35010)) { delitem 35010,countitem(35010); }
				if(countitem(35011)) { delitem 35011,countitem(35011); }
				if(countitem(35012)) { delitem 35012,countitem(35012); }
				if(countitem(35013)) { delitem 35013,countitem(35013); }
				if(countitem(35014)) { delitem 35014,countitem(35014); }
				if(countitem(35015)) { delitem 35015,countitem(35015); }
				/// ARMOR
				if(countitem(34000)) { delitem 34000,countitem(34000); }
				if(countitem(34001)) { delitem 34001,countitem(34001); }
				/// SHIELD
				if(countitem(34200)) { delitem 34200,countitem(34200); }
				if(countitem(34201)) { delitem 34201,countitem(34201); }	
				/// SHOES
				if(countitem(34400)) { delitem 34400,countitem(34400); }
				if(countitem(34401)) { delitem 34401,countitem(34401); }	
				/// GARMENT
				if(countitem(34800)) { delitem 34800,countitem(34800); }
				if(countitem(34801)) { delitem 34801,countitem(34801); }		
				next;
				cutin("verus_ian04",2);
				mes "^00008b[ Salvage Collector Eric ]^000000";
				mes "Salvaging complete!";
				emotion e_cash;
				close2;
				cutin("", 255);
				end;
			}
			else
			cutin("verus_ian03",2);
			mes "^00008b[ Salvage Collector Eric ]^000000";
			mes "You don't have anything to salvage!";
			emotion e_bzz;
			sleep2 1000;
			close2;
			cutin("", 255);			
			end;
			case 3:
			if(countitem(35201) > 0 || countitem(35202) > 0 || countitem(35203) > 0 || countitem(35204) > 0 || countitem(35205) > 0 || countitem(35206) > 0 || countitem(35207) > 0 || countitem(35208) > 0 || countitem(35209) > 0 || countitem(35210) > 0 || countitem(35211) > 0 || countitem(35212) > 0 || countitem(35213) > 0 || countitem(35214) > 0 || countitem(35215) > 0 || countitem(34004) > 0 || countitem(34005) > 0 || countitem(34204) > 0 || countitem(34205) > 0 || countitem(34404) > 0 || countitem(34405) > 0 || countitem(34804) > 0 || countitem(34805) > 0)
			{
				message strcharinfo(0),"Calculating..";
				sleep2 1500;
				message strcharinfo(0),"Salvaging..";
				/// WEAPON				
				if(countitem(35201)) { getitem 7701,9*countitem(35201); }
				if(countitem(35202)) { getitem 7701,9*countitem(35202); }
				if(countitem(35203)) { getitem 7701,9*countitem(35203); }
				if(countitem(35204)) { getitem 7701,9*countitem(35204); }
				if(countitem(35205)) { getitem 7701,9*countitem(35205); }
				if(countitem(35206)) { getitem 7701,9*countitem(35206); }
				if(countitem(35207)) { getitem 7701,9*countitem(35207); }
				if(countitem(35208)) { getitem 7701,9*countitem(35208); }
				if(countitem(35209)) { getitem 7701,9*countitem(35209); }
				if(countitem(35210)) { getitem 7701,9*countitem(35210); }
				if(countitem(35211)) { getitem 7701,9*countitem(35211); }
				if(countitem(35212)) { getitem 7701,9*countitem(35212); }
				if(countitem(35213)) { getitem 7701,9*countitem(35213); }
				if(countitem(35214)) { getitem 7701,9*countitem(35214); }
				if(countitem(35215)) { getitem 7701,9*countitem(35215); }
				/// ARMOR
				if(countitem(34004)) { getitem 7701,9*countitem(34004); }
				if(countitem(34005)) { getitem 7701,9*countitem(34005); }
				/// SHIELD
				if(countitem(34204)) { getitem 7701,9*countitem(34204); }
				if(countitem(34205)) { getitem 7701,9*countitem(34205); }
				/// SHOES
				if(countitem(34404)) { getitem 7701,9*countitem(34404); }
				if(countitem(34405)) { getitem 7701,9*countitem(34405); }			
				/// MUFFLER
				if(countitem(34804)) { getitem 7701,9*countitem(34804); }
				if(countitem(34805)) { getitem 7701,9*countitem(34805); }



				/// WEAPON
				if(countitem(35201)) { delitem 35201,countitem(35201); }
				if(countitem(35202)) { delitem 35202,countitem(35202); }
				if(countitem(35203)) { delitem 35203,countitem(35203); }
				if(countitem(35204)) { delitem 35204,countitem(35204); }
				if(countitem(35205)) { delitem 35205,countitem(35205); }
				if(countitem(35206)) { delitem 35206,countitem(35206); }
				if(countitem(35207)) { delitem 35207,countitem(35207); }
				if(countitem(35208)) { delitem 35208,countitem(35208); }
				if(countitem(35209)) { delitem 35209,countitem(35209); }
				if(countitem(35210)) { delitem 35210,countitem(35210); }
				if(countitem(35211)) { delitem 35211,countitem(35211); }
				if(countitem(35212)) { delitem 35212,countitem(35212); }
				if(countitem(35213)) { delitem 35213,countitem(35213); }
				if(countitem(35214)) { delitem 35214,countitem(35214); }
				if(countitem(35215)) { delitem 35215,countitem(35215); }
				/// ARMOR
				if(countitem(34004)) { delitem 34004,countitem(34004); }
				if(countitem(34005)) { delitem 34005,countitem(34005); }
				/// SHIELD
				if(countitem(34204)) { delitem 34204,countitem(34204); }
				if(countitem(34205)) { delitem 34205,countitem(34205); }	
				/// SHOES
				if(countitem(34404)) { delitem 34404,countitem(34404); }
				if(countitem(34405)) { delitem 34405,countitem(34405); }	
				/// GARMENT
				if(countitem(34804)) { delitem 34804,countitem(34804); }
				if(countitem(34805)) { delitem 34805,countitem(34805); }		
				next;
				cutin("verus_ian04",2);
				mes "^00008b[ Salvage Collector Eric ]^000000";
				mes "Salvaging complete!";
				emotion e_cash;
				close2;
				cutin("", 255);
				end;
			}
			else
			cutin("verus_ian03",2);
			mes "^00008b[ Salvage Collector Eric ]^000000";
			mes "You don't have anything to salvage!";
			emotion e_bzz;
			sleep2 1000;
			close2;
			cutin("", 255);			
			end;
			case 4:
			if(countitem(35401) > 0 || countitem(35402) > 0 || countitem(35403) > 0 || countitem(35404) > 0 || countitem(35405) > 0 || countitem(35406) > 0 || countitem(35407) > 0 || countitem(35408) > 0 || countitem(35409) > 0 || countitem(35410) > 0 || countitem(35411) > 0 || countitem(35412) > 0 || countitem(35413) > 0 || countitem(35414) > 0 || countitem(35415) > 0 || countitem(35415) > 0 || countitem(34006) > 0 || countitem(34007) > 0  || countitem(34008) > 0 || countitem(34009) > 0  || countitem(34010) > 0 || countitem(34206) > 0 || countitem(34207) > 0  || countitem(34208) > 0 || countitem(34209) > 0  || countitem(34210) > 0 || countitem(34406) > 0 || countitem(34407) > 0 || countitem(34408) > 0 || countitem(34409) > 0  || countitem(34410) > 0|| countitem(34806) > 0 || countitem(34807) > 0 || countitem(34808) > 0 || countitem(34809) > 0  || countitem(34810) > 0)
			{
				message strcharinfo(0),"Calculating..";
				sleep2 1500;
				message strcharinfo(0),"Salvaging..";
				/// WEAPON				
				if(countitem(35401)) { getitem 7701,15*countitem(35401); }
				if(countitem(35402)) { getitem 7701,15*countitem(35402); }
				if(countitem(35403)) { getitem 7701,15*countitem(35403); }
				if(countitem(35404)) { getitem 7701,15*countitem(35404); }
				if(countitem(35405)) { getitem 7701,15*countitem(35405); }
				if(countitem(35406)) { getitem 7701,15*countitem(35406); }
				if(countitem(35407)) { getitem 7701,15*countitem(35407); }
				if(countitem(35408)) { getitem 7701,15*countitem(35408); }
				if(countitem(35409)) { getitem 7701,15*countitem(35409); }
				if(countitem(35410)) { getitem 7701,15*countitem(35410); }
				if(countitem(35411)) { getitem 7701,15*countitem(35411); }
				if(countitem(35412)) { getitem 7701,15*countitem(35412); }
				if(countitem(35413)) { getitem 7701,15*countitem(35413); }
				if(countitem(35414)) { getitem 7701,15*countitem(35414); }
				if(countitem(35415)) { getitem 7701,15*countitem(35415); }
				/// ARMOR
				if(countitem(34006)) { getitem 7701,15*countitem(34006); }
				if(countitem(34007)) { getitem 7701,15*countitem(34007); }
				if(countitem(34008)) { getitem 7701,15*countitem(34008); }
				if(countitem(34009)) { getitem 7701,15*countitem(34009); }
				if(countitem(34010)) { getitem 7701,15*countitem(34010); }
				/// SHIELD
				if(countitem(34206)) { getitem 7701,15*countitem(34206); }
				if(countitem(34207)) { getitem 7701,15*countitem(34207); }
				if(countitem(34208)) { getitem 7701,15*countitem(34208); }
				if(countitem(34209)) { getitem 7701,15*countitem(34209); }
				if(countitem(34210)) { getitem 7701,15*countitem(34210); }
				/// SHOES
				if(countitem(34406)) { getitem 7701,15*countitem(34406); }
				if(countitem(34407)) { getitem 7701,15*countitem(34407); }
				if(countitem(34408)) { getitem 7701,15*countitem(34408); }
				if(countitem(34409)) { getitem 7701,15*countitem(34409); }
				if(countitem(34410)) { getitem 7701,15*countitem(34410); }			
				/// MUFFLER
				if(countitem(34806)) { getitem 7701,15*countitem(34806); }
				if(countitem(34807)) { getitem 7701,15*countitem(34807); }
				if(countitem(34808)) { getitem 7701,15*countitem(34808); }
				if(countitem(34809)) { getitem 7701,15*countitem(34809); }
				if(countitem(34810)) { getitem 7701,15*countitem(34810); }

				/// WEAPON
				if(countitem(35401)) { delitem 35401,countitem(35401); }
				if(countitem(35402)) { delitem 35402,countitem(35402); }
				if(countitem(35403)) { delitem 35403,countitem(35403); }
				if(countitem(35404)) { delitem 35404,countitem(35404); }
				if(countitem(35405)) { delitem 35405,countitem(35405); }
				if(countitem(35406)) { delitem 35406,countitem(35406); }
				if(countitem(35407)) { delitem 35407,countitem(35407); }
				if(countitem(35408)) { delitem 35408,countitem(35408); }
				if(countitem(35409)) { delitem 35409,countitem(35409); }
				if(countitem(35410)) { delitem 35410,countitem(35410); }
				if(countitem(35411)) { delitem 35411,countitem(35411); }
				if(countitem(35412)) { delitem 35412,countitem(35412); }
				if(countitem(35413)) { delitem 35413,countitem(35413); }
				if(countitem(35414)) { delitem 35414,countitem(35414); }
				if(countitem(35415)) { delitem 35415,countitem(35415); }
				/// ARMOR
				if(countitem(34006)) { delitem 34006,countitem(34006); }
				if(countitem(34007)) { delitem 34007,countitem(34007); }
				/// SHIELD
				if(countitem(34206)) { delitem 34206,countitem(34206); }
				if(countitem(34207)) { delitem 34207,countitem(34207); }
				if(countitem(34208)) { delitem 34208,countitem(34208); }
				if(countitem(34209)) { delitem 34209,countitem(34209); }	
				if(countitem(34210)) { delitem 34210,countitem(34210); }	
				/// SHOES
				if(countitem(34406)) { delitem 34406,countitem(34406); }
				if(countitem(34407)) { delitem 34407,countitem(34407); }
				if(countitem(34408)) { delitem 34408,countitem(34408); }
				if(countitem(34409)) { delitem 34409,countitem(34409); }
				if(countitem(34410)) { delitem 34410,countitem(34410); }	
				/// GARMENT
				if(countitem(34806)) { delitem 34806,countitem(34806); }
				if(countitem(34807)) { delitem 34807,countitem(34807); }
				if(countitem(34808)) { delitem 34808,countitem(34808); }
				if(countitem(34809)) { delitem 34809,countitem(34809); }
				if(countitem(34810)) { delitem 34810,countitem(34810); }			
				next;
				cutin("verus_ian04",2);
				mes "^00008b[ Salvage Collector Eric ]^000000";
				mes "Salvaging complete!";
				emotion e_cash;
				close2;
				cutin("", 255);
				end;
			}
			else
			cutin("verus_ian03",2);
			mes "^00008b[ Salvage Collector Eric ]^000000";
			mes "You don't have anything to salvage!";
			emotion e_bzz;
			sleep2 1000;
			close2;
			cutin("", 255);			
			end;
		}
	end;
	case 2:
	cutin("verus_ian04",2);
	mes "^00008b[ Salvage Collector Eric ]^000000";
	mes "1 Pet Food Potion = 10 Dragon Soul's";
	sleep2 800;
	next;
	switch(select("Trade Pet Food Potion"))
		{
		if(countitem(7701) > 0)
			{
			message strcharinfo(0),"Calculating..";
			sleep2 1500;
			message strcharinfo(0),"Converting..";
			getitem 36011,countitem(7701)/10;
			delitem 7701,countitem(36011)*10;
				next;
				cutin("verus_ian04",2);
				mes "^00008b[ Salvage Collector Eric ]^000000";
				mes "Convertion complete!";
				emotion e_cash;
				close2;
				cutin("", 255);
				end;
			}
		else
			cutin("verus_ian03",2);
			mes "^00008b[ Salvage Collector Eric ]^000000";
			mes "You don't have anything to convert to!";
			emotion e_bzz;
			sleep2 1000;
			close2;
			cutin("", 255);			
			end;
		}
}
end;
OnInit:
waitingroom("Salvage NPC",0);
}