//================= Hercules Script =======================================
//=       _   _                     _
//=      | | | |                   | |
//=      | |_| | ___ _ __ ___ _   _| | ___  ___
//=      |  _  |/ _ \ '__/ __| | | | |/ _ \/ __|
//=      | | | |  __/ | | (__| |_| | |  __/\__ \
//=      \_| |_/\___|_|  \___|\__,_|_|\___||___/
//================= License ===============================================
//= This file is part of Hercules.
//= http://herc.ws - http://github.com/HerculesWS/Hercules
//=
//= Copyright (C) 2017  Hercules Dev Team
//= Copyright (C) 2017  Smokexyz
//=
//= Hercules is free software: you can redistribute it and/or modify
//= it under the terms of the GNU General Public License as published by
//= the Free Software Foundation, either version 3 of the License, or
//= (at your option) any later version.
//=
//= This program is distributed in the hope that it will be useful,
//= but WITHOUT ANY WARRANTY; without even the implied warranty of
//= MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//= GNU General Public License for more details.
//=
//= You should have received a copy of the GNU General Public License
//= along with this program.  If not, see <http://www.gnu.org/licenses/>.
//=========================================================================
//= Option Master (Item Options NPC @see db/item_options.conf)
//================= Description ===========================================
//= Select from a list of your equipments, one to enhance with up to 5 options.
//= A list of the first 10 options are provided in this npc.
//================= Current Version =======================================
//= 1.0
//=========================================================================

auda_city1,130,130,4	script	Footgear Augmentation	4_F_GEFFEN_FAY,{
	callsub(StartTalking);
	end;
	
OnInit:
	/**
	 * General Configuration
	 */
	/* Chance of the enhancement process to fail. (0 - 99 in percent) */
	.chance_of_failure = 10;
	/* Delete the item on failure? (true/false) */
	.delete_on_failure = true;
	/* Required amount of zeny for a try. */
	.zeny_requirement = 2000000000;
	/* Minimum amount of the bonus value. 
	 * For negative effects or certain bonuses that require negative values
	 * Maximum possible value is -INT16_MAX)
	 */
	.minimum_bonus_amount = 1; // usually used with delay bonus options, although not provided in the script.
	/* Maximum amount of the bonus value. 
	 * Maximum possible value is INT16_MAX */
	.maximum_bonus_amount = 15;
	/* Disable selection of bonus value (true/false) */
	.enable_random_bonus = true;
	
	/* Item Option Descriptions */
	setarray(.options$[0], "Max HP %");
	/* Item Option Constants */
	setarray(.option_constants[0], VAR_MAXHPPERCENT);
waitingroom "Footgear Augment",0;
	end;
	
StartTalking:
	.@name$ = _("[ ^990099Footgear Augmentation^000000 ]");
	disable_items();
	mes(.@name$);
	mesf("Greetings %s", strcharinfo(PC_NAME));
	mes("Looks like your Footgear could use some advancement! hoho~");
	next();
	if (.enable_random_bonus) {
		mes(.@name$);
		mes("Augmentation values are added randomly per slot!");
		mes("So if you wind up with a low amount of bonus, don't hesitate to override the slot.");
		next();
		mes(.@name$);
		mes("Maximum Rate: 15%");
		mes("Minimum Rate: 1%");
		next();
	}
	mes(.@name$);
	mes("Would you like to try my augmentation services?");
	next();
	if (select("Of course!", "Meh, not right now.") == 2) {
		mes(.@name$);
		mes("Alright then, catch you later.");
		close();
	}
	// Build the Menu.
	setarray(.@position$[1], " ", "Footgear" );
	.@menu$ = "";
	.@menu$ = ((getequipisequiped(EQI_Footgear)) ? getequipname(2) : .@position$[2] + "-[Not equipped]") + ":";
	//.@menu$ += ((getequipisequiped(2)) ? getequipname(2) : .@position$[2] + "-[Not equipped]") + ":";
	//for (.@i = 1; .@i <= 5; ++.@i)
	//	.@menu$ += ((getequipisequiped(.@i)) ? getequipname(.@i) : .@position$[.@i] + "-[Not equipped]") + ":";
	// Select the part.
	.@equip_index = select(.@menu$)+5;
	
	// Check if it's worn.
	if (!getequipisequiped(.@equip_index)) {
		mes(.@name$);
		mes("It seems like you're trying to cheat my augmentation, don't dissapoint me!");
		close();
	// Check if it allows options
	} else if (!getequipisenableopt(.@equip_index)) {
		mes(.@name$);
		mes("Too bad.. your equipment is so rusty and weak.");
		close();
	// Check if equipment is identified.
	} else if (!getequipisidentify(.@equip_index)) {
		mes(.@name$);
		mes("You think i'm too dumb? Get out!");
		close();
	}
	
	// Present a list of currently infused options.
	do {
		.@menu$ = "";
		.@used = false;
		// Build the menu of current options.
		for (.@i = 1; .@i <= 2; ++.@i) {
			// call getequipoption(<equip_index>, <type>, <slot>);
			// if the return is <0, it's a script error.
			// if the return is 0, the slot is empty.
			// if the return is >0, the slot is unavailable.
			.@opt = getequipoption(.@equip_index, .@i, IT_OPT_INDEX);
			if (.@opt > 0)
				.@menu$ += (.@i) + ") " + .options$[.@opt - 1] + ":";
			else
				.@menu$ += (.@i) + ") ^999999Empty^000000" + ":";
		}
		// Option Slot is the actual option slot 0-MAX_ITEM_OPTIONS (@see mmo.h)
		.@option_slot = select(.@menu$);
		
		// Check for used slot and request user action if found.
		if (getequipoption(.@equip_index, .@option_slot, IT_OPT_INDEX) > 0) {
			mes(.@name$);
			mes("This is already occupied!");
			if (select("^990000Override the slot.^000000", "Choose again.") == 2)
				.@used = true;
			next();
		}
	} while (.@used); // loop if the slot is not to be overridden
	
	// Present a list of available bonuses.
	mes(.@name$);
	mes("Select your desired augmentation:");
	next();
	// Build the Options!
	.@menu$ = "";
	for (.@i = 0; .@i < getarraysize(.options$); ++.@i)
		.@menu$ += (.@i + 1) + ") " + .options$[.@i] + ":";
		
	do {
		// Select the options!
		.@option_variable = select(.@menu$);
		next();
		mes(.@name$);
		mesf("You chose ^009900%s^000000!", .options$[.@option_variable - 1]);
		mes("Is this your final choice?");
		next();
	} while (select("Yes", "Wait, let me choose again.") == 2);
	
	// Select a bonus or randomise.
	if (.enable_random_bonus) {
		.@value = rand(.minimum_bonus_amount,.maximum_bonus_amount);
	} else {
		do {
			mes(.@name$);
			mesf("Please input the bonus amount of ^009900%s^000000 you want to add!", .options$[.@option_variable - 1]);
			mesf("(Min: %d, Max: %d)", .minimum_bonus_amount, .maximum_bonus_amount);
			.@ok = input(.@value, .minimum_bonus_amount, .maximum_bonus_amount);
			next();
		} while (.@ok);
	}
	
	// If there's a chance of failure, inform the user.
	if (.chance_of_failure) {
		mes(.@name$);
		mes("Despite of my great ability to develop augmentations");
		mes("I'll have you know...");
		mesf("There's a ^990000%d%% chance of failure^000000.", .chance_of_failure);
		mes("it can be quite unstable sometimes..");
		next();
		mes(.@name$);
		if (.delete_on_failure) {
			mes("If I fail, your item will break and it ^990000will be destroyed^000000!");
		}
		mes("Do you still wanna seek true power?");
		next();
		if (select("Hell yeah!", "Ohhh Hell no!") == 2) {
			mes(.@name$);
			mes("Very well, Come back when you are ready!");
			close();
		}
		next();
	}
	
	// If there's a Zeny Requirement, perform the usual.
	if (.zeny_requirement > 0) {
		mes(.@name$);
		mesf("A small fee is required to proceed our augmentation procedure");
		mesf("I'm very sure you can handle it");
		mesf("For that, i need %dZ.", .zeny_requirement);
		sleep2 1000;
		mesf("Oh wait..");
		sleep2 1000;
		mesf("Also..");
		next();
		mes(.@name$);
		mesf("3x Chrysoberyl");
		mesf("3x Magic Fragments");
		mesf("3x Cold-Forged Orb");
		mesf("1x Anathema");
		mesf("1x Cluster");
		mesf("1x Macadam");
		mesf("500 Platinum Coin");
		switch(select("I have what you need..","Hold on.")){
			case 1:
			if (Zeny < .zeny_requirement && countitem(36000) < 3 && countitem(36001) < 3 && countitem(36002) < 3 && countitem(36007) < 1 && countitem(36008) < 1 && countitem(36009) < 1 && countitem(37009) < 500) {
			next;
			mes(.@name$);
			mes("You don't have everything i asked!");
			close;
			}
		else
		delitem 36000,1;
		delitem 36001,1;
		delitem 36002,1;
		delitem 36007,1;
		delitem 36008,1;
		delitem 36009,1;
		delitem 37009,500;
		Zeny -= .zeny_requirement;
			case 2:
			mes(.@name$);
			mes("Well, as i expected..");
			close();
		}		
	}
	// Check if there's a chance of failure, set and roll the dice.
	if (.chance_of_failure && rand(100) <= .chance_of_failure) {
		next;
		mes(.@name$);
		mes("^990000I failed!^000000");
		mes("*poof*");
		// Delete the item if flagged.
		if (.delete_on_failure)
			failedrefitem(.@equip_index); // Simulate failed refinement of the item and delete.
	} else {
		// Set the Item Option bonuses in the slot see db/item_options.conf for more info.
		next;
		announce ("Footgear Augmentation: Wow ! [ "+strcharinfo(0)+" ] just got his footgear augmented! ALL HAIL!", bc_yellow);
		setequipoption(.@equip_index, .@option_slot, .option_constants[.@option_variable - 1], .@value);
		mes(.@name$);
		mes("^009900Chaaaa Ching!^000000");
		mes("Augmentation was a successful!");
		mes("Now go and show the world the power that you hold!");

	}
	next();
	mes(.@name$);
	mes("Farewell!");
	close();
}
