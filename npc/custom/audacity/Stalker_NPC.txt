
auda_city1,214,201,1	script	Stalker	4_M_SHADOWCHASER,{
set .@skill_level,getskilllv( "RG_PLAGIARISM" );

mes "I provide skill to Stalker Class.";

if( !.@skill_level ){
	mes "But only who have ^FF0000Plagiarism skill^000000 can use this services.";
}else{
	if( select( ( CLONE_SKILL && CLONE_SKILL_LV )?"Un-Learn Skill":"Learn Skill","Close" ) == 1 ){
		if( CLONE_SKILL && CLONE_SKILL_LV ){
			mes "Unlearned Copied Skill";
			set CLONE_SKILL_LV,0;
			skill CLONE_SKILL,CLONE_SKILL_LV,0;
			set CLONE_SKILL,0;
		}else{
			mes "Please select a Skills";
			next;
			set .@i,select( .menu$ ) - 1;
			if( .@skill_level > .skill_max_level[.@i] )
				set .@skill_level,.skill_max_level[.@i];
			mes "Skill : ^0055FF"+.skill_name$[.@i]+"^000000";
			mes "Level : ^0055FF"+.@skill_level+"^000000";
			mes "Cost : ^0055FF"+( ( .skill_cost[.@i] )? .skill_cost[.@i]+" Zeny":"Free" )+"^000000";
			mes " ";
			mes "^777777( Plagiarism Affect Skill Level )^000000";
			if( select( "Confirm","Cancel" ) == 1 ){
				if( .skill_cost[.@i] ){
					if( Zeny >= .skill_cost[.@i] ){
						set Zeny,Zeny - .skill_cost[.@i];
					}else{
						mes "Not enough Zeny.";
						close;
					}
				}
				set CLONE_SKILL,.skill_id[.@i];
				set CLONE_SKILL_LV,.@skill_level;
				mes "Learned Skill ^0055FFLv"+CLONE_SKILL_LV+" "+.skill_name$[.@i]+"^000000";
				next;
				mes "You will be logged out to finalize the process!";
				atcommand "@kick "+strcharinfo(0);
				skill CLONE_SKILL,CLONE_SKILL_LV,0;
			}
		}
	}
}
//dispbottom "Skill ID "+CLONE_SKILL+" | Level : "+CLONE_SKILL_LV;
close;

OnInit:
// Skill Display Names
setarray .skill_name$,"Jupiter Thunder","Sacrifice","Acid Demonstration","Storm Gust","Lord of Vermillion","Flying Side Kick","Bowling Bash","Enchant Deadly Poison";
// Skill ID
setarray .skill_id,84,368,490,89,85,421,62,378;
// Skill Max Learn-able Level
setarray .skill_max_level,5,3,10,7,7,7,7,1;
// Skill Cost in Zeny
setarray .skill_cost,100000000,100000000,100000000,100000000,100000000,100000000,100000000,100000000;

set .skill_size,getarraysize( .skill_name$ );
while( .@i < .skill_size ){
	set .menu$,.menu$ + "[ ^0055FFLv "+.skill_max_level[.@i]+"^000000 ] "+ .skill_name$[.@i] +":";
	set .@i,.@i + 1;
}
waitingroom "Stalker Copy NPC",0;
end;
}