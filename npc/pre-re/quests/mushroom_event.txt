//===== Hercules Script ======================================
//= Find the Mushroom
//===== By: ==================================================
//= Mysterious
//===== Current Version: =====================================
//= 3.6a
//===== Description: =========================================
//= Find the Mushroom - random amount of Mushrooms spawns in random maps.
//= Players need to find these mushrooms and kill them to gain prizes!
//===== Additional Comments: =================================
//= 3.0 Fully Functional with Rewritten script. [Mysterious]
//= 3.6a Slightly edited. [Euphy]
//============================================================

auda_city1,114,217,6	script	Kill the Fairy	FAIRY_1,{
	mes "[ Kill the Fairy ]";
	if (!.status)
		mes "There is no event at the moment!";
	else {
		mes "There are "+.Spawn+" Fairy(s) left in "+.map$+"!";
		mes "Find and kill the Fairy(s) to gain "+getitemname(.Prize)+"!";
	}
	if (.status || getgmlevel() < .GM) close;
	mes "Start the event?";
	next;
	if(select("- No", "- Yes") == 1) close;
	donpcevent strnpcinfo(NPC_NAME)+"::OnMinute10";
	mes "[ Kill The Fairy ]";
	mes "Event started!";
	close;

OnInit:
	set .Prize,36004;	// Reward item ID
	set .amount,1;	// Reward item amount
	set .GM,99;	// GM level required to access NPC
	setarray .maps$[0],"auda_city1"; // Possible maps
	end;

OnMinute10:	// Start time (every hour)
	if (.status) end;
	set .status,1;
	set .Spawn,rand(20,50);	// How many Mushrooms should spawn?
	set .map$,.maps$[rand(getarraysize(.maps$))];
	//killmonster(.map$, "all");
	monster .map$,0,0,"HELP!",21036,.Spawn,strnpcinfo(NPC_NAME)+"::OnMobKilled";
	announce "Kill the Fairy : Total of "+.Spawn+" Fairy(s) have been spawned in "+.map$+"!",0;
	sleep 2500;
	announce "Kill the Fairy : Every Fairy you kill will give you "+getitemname(.Prize)+"!",0;
	end;

OnMobKilled:
	set .Spawn, .Spawn - 1;
	if (playerattached() != 0) {
		getitem .Prize, .amount;
		if (.Spawn)
			announce "[ "+strcharinfo(PC_NAME)+" ] has killed a Fairy. There are now "+.Spawn+" Fairy(s) left.",bc_map;
	}	
	if (!.Spawn) {
		announce "Kill the Fairy Event has ended. All the Fairy(s) have been killed.",0;
		set .status,0;
	}
	end;
}
