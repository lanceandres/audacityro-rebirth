//================================
//== Script by Aeromesi ==========
//================================
//== Edited by 2020 Ragnarok =====
//================================
-	script	supreqadmin	-1,{

OnPCLoginEvent:

.Admin = 99;
.GmMast = 50;

	if(getgmlevel() >=.Admin) {
	mes "[Support Request]";
	mes "Would you like to accept support requests?";
	next;
	switch(select("Yes:No:Cancel")) {
	case 1:
	if(getgmlevel() >=.Admin) {
	announce "Admin [ "+strcharinfo(0)+" ] has just signed in and accepting support requests.",bc_all,0x00FF66;
	mes "[Support Request]";
	mes "You accepted requests.";
	close;
	end;
}
	case 2:
	if(getgmlevel() >=.Admin) {
	announce "Admin [ "+strcharinfo(0)+" ] has just signed in and not accepting support requests right now, might be busy.",bc_all,0x00FF66;
	mes "[Support Request]";
	mes "You are not accepting requests at this time.";
	close;
	end;
}
	case 3:
	if(getgmlevel() >=.Admin) {
	mes "[Support Request]";
	mes "You are now lurking freely.";
	close;
	end;
}
}
} else 
	{ if(getgmlevel() == .GmMast) {
	mes "[Support Request]";
	mes "Would you like to accept player questions through the whisper system?";
	next;
	switch(select("Yes:No")) {
	case 1:
	if(getgmlevel()  == .GmMast) {
	announce "GM [ "+strcharinfo(0)+" ] has just signed in and accepting questions, reports and concerns, please whisper them.",bc_all,0x00FF66;
	mes "You are accepting inquiries by players at this time.";
	close;
	end;
}
	case 2:
	if(getgmlevel()  == .GmMast) {
	announce "GM [ "+strcharinfo(0)+" ] has just signed in and not accepting questions, reports or concerns at this time, might be busy.",bc_all,0x0FF66;
	mes "You are not accepting inquiries by players at this time.";
	close;
	end;
	}
}
}
}
}