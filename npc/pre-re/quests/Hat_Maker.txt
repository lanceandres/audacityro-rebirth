auda_city1,273,316,3	script	Hat Generator	4_M_PEPPERROTI,{
set .topHG2,rand(5001,5859);
mes "[KRO Costume Maker]";
mes " It cost 1 Audacity Coin to get 1 Random KRO Headgear!";
mes " Would you like to try your luck?";
next;
switch (select("Yes","No")){
case 1:
if (countitem(36005) < 1 ){
mes "[KRO Costume Maker]";
mes "You don't have any Audacity Coins!";
close;
end;
}
else
delitem 36005,1;
mes "[KRO Costume Maker]";
mes "Alright rolling the headgears!";
next;
mes "Almost there..";
next;
mes "Just a bit more..";
next;
mes "FINISHED";
getitem .topHG2,1;
announce "KRO Costume Maker: Congratulations "+strcharinfo(0)+" your reward is [ "+getitemname(.topHG2)+" ]!",bc_map;
close;
case 2:
mes "[KRO Costume Maker]";
mes "Sad.. Farewell!";
close;
}
end;
OnInit:
	waitingroom "Hat Generator",0;
end;
}